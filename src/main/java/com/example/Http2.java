package com.example;

import com.google.gson.annotations.SerializedName;

public class Http2 {
    public int http2;
    public String protocol;
    @SerializedName (value = "user_agent")
    public String userAgent;

    @Override
    public String toString() {
        return String.format(" - Http 2 : %d%n - protocol : %s%n - userAgent: %s%n", this.http2, this.protocol, this.userAgent);
    }
}