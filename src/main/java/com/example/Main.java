package com.example;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandlers;

import com.google.gson.Gson;

public class Main {

    public static void main(String[] args) throws Exception {
        HttpClient client = HttpClient.newHttpClient();

        HttpRequest request = HttpRequest.newBuilder(URI.create("https://http2.pro/api/v1"))
                // .header("accept", "application/json")
                .GET()
                // .POST(HttpRequest.BodyPublishers.noBody())
                .build();

        HttpResponse<String> response = client.send(request, BodyHandlers.ofString());

        // https://github.com/google/gson/blob/main/UserGuide.md
        Gson gson = new Gson();
        Http2 obj = gson.fromJson(response.body(), Http2.class);
        System.out.println("Response :");
        System.out.println(obj);
    }
}
